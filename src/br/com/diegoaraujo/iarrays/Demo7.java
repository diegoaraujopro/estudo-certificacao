package br.com.diegoaraujo.iarrays;

class Demo7 {
    public static void main (String args[]) {
        String[] valores = new String[2];
        valores[0] = "Certification";
        valores[1] = "Java";
        Object vars[] = (Object[]) valores;
        vars[1] = "Diego";
        System.out.println(vars[1].equals(valores[1])); // True 
    }
}


 