/**
Quais das maneiras adiante são declarações e inicializações válidas para um array?
a) int[] array = new int[10]; // ok
b) int array[] = new int[10]; // ok
c) int[] array = new int[]; // inválido, pois não foi especificado o tamanho do array
d) int array[] = new int[]; // inválido, pois não foi especificado o tamanho do array
e) int[] array = new int[2]{1, 2}; // inválido, pois foi especificado o tamanho do array e os valores na mesma linha. Ou um ou outro.
f) int[] array = new int[]{1, 2}; // ok
g) int[] array = int[10]; // inválido, pois falou a declaração new na hora de instanciar.
h) int[] array = new int[1, 2, 3]; // inválido, pois não inicializa um array dessa forma.
i) int array[] = new int[1, 2, 3]; // inválido, pois não inicializa um array dessa forma.
j) int array[] = {1, 2, 3}; // ok
*/

package br.com.diegoaraujo.iarrays;

class Demo8 {
    public static void main(String[] args) {
        int[] array = new int[10]; // ok
        int array2[] = new int[10]; // ok
        int[] array3 = new int[]; // inválido, pois não foi especificado o tamanho do array
        int array4[] = new int[]; // inválido, pois não foi especificado o tamanho do array
        int[] array5 = new int[2]{1, 2}; // inválido, pois foi especificado o tamanho do array e os valores na mesma linha. Ou um ou outro.
        int[] array6 = new int[]{1, 2}; // ok
        int[] array7 = int[10]; // inválido, pois falou a declaração new na hora de instanciar.
        int[] array8 = new int[1, 2, 3]; // inválido, pois não inicializa um array dessa forma.
        int array9[] = new int[1, 2, 3]; // inválido, pois não inicializa um array dessa forma.
        int array10[] = {1, 2, 3}; // ok
    }
}