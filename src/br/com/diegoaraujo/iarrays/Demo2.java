package br.com.diegoaraujo.iarrays;

class Demo2 {
    public static void main (String args[]) {
        int x[] = new int[30];        // A
        int y[] = new int[3] {0,3,5}; // B Não compila, pois não é pode colocar o tamanho e popular desta forma.
    }
}