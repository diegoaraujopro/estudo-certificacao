package br.com.diegoaraujo.wrapper;

public class TesteWrapper {
	
	public static void main (String args[]) {
		Integer referencia = Integer.valueOf("3"); // Autoboxing
		referencia++; // Unboxing
		System.out.println(referencia);
	}

}
