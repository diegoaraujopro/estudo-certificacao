package br.com.diegoaraujo.iarraysmulti;

class Demo1 {
    public static void main (String args[]) {
        int zyx[][]=new int[3]; // Não compila, pois foi declarado um array de 2 dimensões, porém na hora de incializar, foi passado um de 1 dimensão.
        int[]x=new int[20];
        int[]y=new int[10];
        int[]z=new int[30];
        zyx[0]=x;
        zyx[1]=y;
        zyx[2]=z;
        System.out.println(zyx[2].length);
    }
}