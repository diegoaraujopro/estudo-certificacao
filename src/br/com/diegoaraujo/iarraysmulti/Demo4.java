package br.com.diegoaraujo.iarraysmulti;

class Demo4 {
    public static void main (String args[]) {
        int[] id = new int[10];
        id[0] = 1.0; // Não compila. Sem cast (int), não vai compilar
        int[10][10] tb = new int[10][10]; // Não compila. Não se declara o tamanho do array do lado esquerdo
        int[][][] cb = new int[][][]; // Não compila. Não se pode instanciar um array, sem informa a primeira dimensão
}
}