package br.com.diegoaraujo.switchcommand;

class Demo4 {

    public static void main(String ... args) {
        int count = args.length;
        switch (count) {
            case 0 { // Não compila. Sintaxe do case é case :
                System.out.println("---");
                break;
            } case 1 { // Não compila. Sintaxe do case é case :

            } case 2 { // Não compila. Sintaxe do case é case :
                System.out.println("Ok");
            } default { // Não compila. Sintaxe do case é case :
                System.out.println("Default");
            }
        }
    }
}