package br.com.diegoaraujo.switchcommand;

class Demo2 {
    public static void main (String args[]) {
        int t2 = 1;
        int t = args.length;

        switch(t) {
            case t2: // Não compila, pois espera uma constante (tipo final por exemplo) e não uma variável não constante
                System.out.println("One");
                break;
            default:
                System.out.println("args ???");
        }
    }
}