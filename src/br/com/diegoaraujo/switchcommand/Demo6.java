package br.com.diegoaraujo.switchcommand;

class Demo6 {
    public static void main (String args []) {
        switch(10) {
            case 10: 
                System.out.println("a");
                break;
                System.out.println("b"); // Não compila. Error unreachable statement (Nunca será executado/lido)
            default:
                System.out.println("c");
            case 11:
                System.out.println("d");
        }
    }
}