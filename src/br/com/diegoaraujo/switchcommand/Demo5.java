package br.com.diegoaraujo.switchcommand;

class Demo5 {
    public static void main (String args []) {
        switch(10) {
            case < 10: // Não compila, expressão não é válida
                System.out.println("<");
            default:
                System.out.println("=");
            case > 10: // Não compila, expressão não é válida
                System.out.println(">");
        }
    }
}