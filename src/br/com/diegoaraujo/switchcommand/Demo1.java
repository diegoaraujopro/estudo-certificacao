package br.com.diegoaraujo.switchcommand;

class Demo1 {
    public static void main (String args[]) {
        int t = args.length;
        switch (t) {
            case 1:
                System.out.println("One");
            case 2: 
                System.out.println("Two");
            default:
                System.out.println("+++");
        }
    }
}