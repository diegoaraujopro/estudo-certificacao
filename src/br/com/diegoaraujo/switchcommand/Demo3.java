package br.com.diegoaraujo.switchcommand;

class Demo3 {

    public static void main(String args[]) {
        switch("guilherme") {
            case "guilherme":
                System.out.println("guilherme");
                break;
            case "42":
                System.out.println("42");
            default:
                System.out.println("Mario");
        }
    }

}