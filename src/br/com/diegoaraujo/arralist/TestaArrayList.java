package br.com.diegoaraujo.arralist;

import java.util.ArrayList;
import java.util.List;

import br.com.diegoaraujo.model.Conta;

public class TestaArrayList {

	public static void main (String args[]) {
		Conta c1 = new Conta(27278, 155533);
		Conta c2 = new Conta(27278, 155533);
		
		System.out.print("c1 - " + c1 );
		System.out.println(" é igual a c2 - " + c2 + " ?" );
		System.out.println(c1.equals(c2)); // agora é true, pois o equals foi sobreescrito
		
		List<Conta> contas = new ArrayList<Conta>(); 
		contas.add(c1);
		System.out.println("Contas contém c2? " + contas.contains(c2));
		
	}
}
