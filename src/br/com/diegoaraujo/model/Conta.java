package br.com.diegoaraujo.model;


public class Conta {

	private int agencia;
	private int numero;
	
	public Conta(int agencia, int numero) {
		this.agencia = agencia;
		this.numero = numero;
	}
	
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	
	public int getAgencia() {
		return this.agencia;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
 	public int getNumero() {
 		return this.numero;
 	}
 	
 	@Override
 	public String toString() {
 		return "Conta -> Agência: " + this.agencia + " Número: " + this.numero;
 	}
 	
 	@Override // não é obrigatório informa a anotação
 	public boolean equals(Object obj) {
 		Conta c = (Conta) obj;
 		if (this.agencia != c.agencia) 
 			return false;
 		else if (this.numero != c.numero)
 			return false;
 		
 		return true;
 	}
 	
 }
