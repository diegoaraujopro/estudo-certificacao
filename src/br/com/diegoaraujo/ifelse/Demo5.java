package br.com.diegoaraujo.ifelse;

class Demo5 {
    public static void main(String[]args) {
        String name = args[0];
        if(name.equals("guilherme"))
            System.out.println(name);
            System.out.println("good");
        else // Não compila, pois dá erro no else já que o if sem chaves {} só reconhece 1 comando após ele.
            System.out.println("better");
            System.out.println(name);

    }
}