package br.com.diegoaraujo.ifelse;

class Demo3 {
    public static void main(String args[]) {
        int qt = 15;
        if (qt = 15) // Não compila, pois qt = 15 não é uma expressão lógica (true or false)
            System.out.println("Yes");
        else
            System.out.println("No");    
    }
}