package br.com.diegoaraujo.ifelse;

class Demo4 {
    public static void main(String [] args) {
        if (args.length == 1)
            System.out.println("One");
        elseif(args.length == 2) // Não compila, else if é separado
            System.out.println("Two");
        elseif(args.length == 3) // Não compila, else if é separado
            System.out.println("Three");
        else
            System.out.println("Four");    
    }
}