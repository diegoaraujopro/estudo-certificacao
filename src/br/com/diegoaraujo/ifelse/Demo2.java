package br.com.diegoaraujo.ifelse;

class B {
    final boolean valor = false;
}

class Demo2 {
    public static void main(String args[]) {
        B b = new B();
        if (b.valor = true) // Não compila, pois valor é final, portanto, não é possível atribuir outro valor a ela.
            System.out.println("Uhuu true");

    }
}