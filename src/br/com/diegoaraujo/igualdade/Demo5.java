package br.com.diegoaraujo.igualdade;

class B extends C {

}

class D {
    int x;
}

class C {
    int x;
    public boolean equals(Object c) {
        return c.x == x; // Não compila, pois não reconhece um Object como C. Erro na sobreescrita do método equals.
    }
}


class Demo5 {
    public static void main (String[] args) {
        C a = new C();
        C b = new D(); // D não pode ser convertido para C.
        a.x = 1;
        b.x = 1;
        System.out.println(a == b);
        System.out.println(a.equals(b)); 
    }
}