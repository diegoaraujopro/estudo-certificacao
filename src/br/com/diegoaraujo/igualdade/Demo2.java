package br.com.diegoaraujo.igualdade;

class Demo2 {

    public static void main(String ... args) {
        String s1 = "ab";
        String s2 = s1.substring(0,1) + s1.substring(1,2);
        System.out.println(s1 == s2);
        System.out.println(s1 == "" + s2);
    }
}