package br.com.diegoaraujo.igualdade;

class B extends C {

}

class C {
    int x;
    public boolean equals(C c) {
        return c.x == x;
    }
}


class Demo4 {

    public static void main(String ... args) {
        C a = new C();
        C b = new B();
        a.x = 1;
        b.x = 1;
        System.out.println(a == b); // false, pois estão em referencias diferentes
        System.out.println(a.equals(b)); // true, pois os valores de x são igual

    }

}