package br.com.diegoaraujo.igualdade;

class Demo1 {

    public static void main(String ... args) {
        String s1 = "ab";
        String s2 = "a" + "b";
        System.out.println(s1 == s2);
        System.out.println(s1 == "" + s2);
    }
}