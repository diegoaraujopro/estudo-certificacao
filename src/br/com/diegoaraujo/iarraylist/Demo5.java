package br.com.diegoaraujo.iarraylist;

import java.util.ArrayList;

class Demo5 {
    public static void main (String args[]) {
        ArrayList<String> l = new ArrayList<String>();
        l.add("a");
        l.add("b");
        l.add(1,"amor");
        l.add(3,"love");
        System.out.println(l);
        String[] array = l.toArray(); // Não compila, pois nãon consegue converter um Object[] para String[], faltou .toArray(new Strin[4]).
        System.out.println(array[2]);
 
    }
}