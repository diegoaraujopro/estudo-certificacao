package br.com.diegoaraujo.iarraylist;

// Não compila, pois falta o import da class ArrayList (import java.util.ArrayList;)


class Demo1 {
    public static void main (String args[]) {
        ArrayList<String> c = new ArrayList<>();
        c.add("a");
        c.add("b");
        System.out.println(c.remove("a"));
    }
}