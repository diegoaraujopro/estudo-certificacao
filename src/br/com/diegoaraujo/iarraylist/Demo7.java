package br.com.diegoaraujo.iarraylist;

import java.util.ArrayList;

class Demo7 {
    public static void main (String args[]) {
        ArrayList<String> a = new ArrayList<String>();
        a.add("a", 0); // Não compila. O método é add(int index, String nome)
        a.add("b", 0);
        a.add("c", 0);
        a.add("d", 0);
        System.out.println(a.get(0));
        System.out.println(a.get(1));
        System.out.println(a.get(2));
        System.out.println(a.get(3));
    }
}